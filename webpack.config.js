// webpack v4
const path = require("path");

const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const webpack = require("webpack");

module.exports = {
  entry: {
    main: "./theme/index.js"
  },
  output: {
    path: path.resolve(__dirname, "public/dist"),
    filename: "bundle.js"
  },
  devtool: "",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          "css-loader?url=false",
          "sass-loader",
          "postcss-loader"
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin("public/dist", {}),
    new MiniCssExtractPlugin({
      filename: "style.css"
    }),
    new HtmlWebpackPlugin({
      inject: false,
      hash: true,
      template: "./theme/index.html",
      filename: "index.html"
    }),
    new HtmlWebpackPlugin({
      inject: false,
      hash: true,
      template: "./theme/interior.html",
      filename: "interior.html"
    }),
    new HtmlWebpackPlugin({
      inject: false,
      hash: true,
      template: "./theme/card-test.html",
      filename: "card-test.html"
    }),
    new HtmlWebpackPlugin({
      inject: false,
      hash: true,
      template: "./theme/interior01.html",
      filename: "interior01.html"
    }),
    new HtmlWebpackPlugin({
      inject: false,
      hash: true,
      template: "./theme/interior01.html",
      filename: "interior01.html"
    }),
    new webpack.LoaderOptionsPlugin({
      debug: true
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, "theme"),
    compress: true,
    port: 8888
  }
};
