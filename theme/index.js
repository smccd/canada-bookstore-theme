import "./scss/style.scss";

let headerContent = `
<div class='flex-header'>
    <div class="col-md-3">
        <span class='csm-logo'><img
                src="http://bookstore.canadacollege.edu/SiteImages/13-SchoolImages/13-Logos/13-Bookstore_Logo_White-2021-01.png"
                alt="Cañada College Bookstore Logo" /></span>
    </div>
    <div class="col-md-9">
        <form method='get' action="http://insitestore2.mbsbooks.com/canadasandbox/merchlist.aspx">
            <div class="input-group" style="max-width: 250px; float: right;">
                <input name="searchtype" type="hidden" value="all">
                <input name="txtSearch" type="text" class="form-control" aria-label="Search"
                    placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" aria-label="Search"><svg class="search-group-icon"
                            aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search"
                            class="svg-inline--fa fa-search fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512">
                            <path fill="currentColor"
                                d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z">
                            </path>
                        </svg></button>
                </span>
            </div>
        </form>
    </div>
</div>
`;

let footerContent = `
<div class="can-footer">
    <div class="container">
        <div style="text-align: center; margin-bottom: 20px;">
            <h5 class="pv2 toplink"><a title="Back to top" href="#top">Back to Top</a></h5>
        </div>
        <div class="row">
            <div class="col-sm-9">
                <div style="border: solid 1px grey; border-radius: 7px; padding: 10px; margin-bottom: 1rem;">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <ul class="footer-list">
                                <li class="mb2"><a href="https://canadacollege.edu/search/azindex.php">A-Z Site
                                        Index</a></li>
                                <li class="mb2"><a href="https://smccd.instructure.com/">Canvas</a></li>
                                <li class="mb2"><a href="http://directory.smccd.edu/allemp.php?college=Canada">Employee
                                        Directory</a></li>
                                <li class="mb2"><a href="http://mail.my.smccd.edu/">Student Email</a></li>
                                <li class="mb2"><a href="https://webschedule.smccd.edu/">WebSchedule</a> <span
                                        style="color: #cfcfcf;">/</span> <a
                                        href="https://websmart.smccd.edu/">WebSmart</a></li>
                                <li class="mb2"><a href="https://canadacollege.edu/search/index.php">Search</a></li>
                            </ul>
                            <div class="visible-xs"
                                style="margin-top: 20px; margin-bottom: 20px; border-top: 1px solid grey;">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 borderLeft">
                            <ul class="footer-list">
                                <li class="mb2"><a href="https://canadacollege.edu/admissions/calendar.php">Academic
                                        Calendar</a></li>
                                <li class="mb2"><a href="http://bookstore.canadacollege.edu/home.aspx">Bookstore</a>
                                </li>
                                <li class="mb2"><a href="https://canadacollege.edu/about/campusmap.php">Campus Map</a>
                                </li>
                                <li class="mb2"><a href="https://catalog.canadacollege.edu/">Catalog</a></li>
                                <li class="mb2"><a href="https://canadacollege.edu/library/">Library</a></li>
                                <li class="mb2"><a href="https://canadacollege.edu/catalogschedule/">Schedule</a></li>
                            </ul>
                            <div class="visible-xs"
                                style="margin-top: 20px; margin-bottom: 20px; border-top: 1px solid grey;"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4 borderLeft">
                            <ul class="footer-list">
                                <li class="mb2"><a href="https://canadacollege.edu/accreditation/">Accreditation</a>
                                </li>
                                <li class="mb2"><a
                                        href="http://smccd.edu/publicsafety/emergencypreparedness.php">Emergency
                                        Information</a></li>
                                <li class="mb2"><a href="https://canadacollege.edu/cares/">Report Concerning Behavior
                                        (CARES Report)</a></li>
                                <li class="mb2"><a href="http://www.smccd.edu/titleix/">Sexual Misconduct (Title IX)</a>
                                </li>
                                <li class="mb2"><a href="http://smccd.edu/accessibility/">Website Accessibility</a></li>
                                <li class="mb2"><a href="https://canadacollege.edu/housing/">Housing</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 ph4" style="text-align: center;">
                <a style="text-align:left" href="https://canadacollege.edu/admissions/"
                    class="center-block btn btn-primary btn-sm mb2"> <span class="fa fa-user-circle"
                        aria-hidden="true"></span> &nbsp;Apply</a>
                <a style="text-align:left" href="https://canadacollege.edu/outreach/campusTour.php"
                    class="center-block btn btn-primary btn-sm mb2"><span class="fa fa-map-signs"
                        aria-hidden="true"></span> &nbsp;Schedule a Tour</a>
                <a style="text-align:left" href="https://foundation.smccd.edu/donate.php"
                    class="center-block btn btn-primary btn-sm mb2"><span class="fa fa-star" aria-hidden="true"></span>
                    &nbsp;Donate</a>
            </div>
        </div>
    </div>
    <div class="sub-footer">
        <div class="container">
            <div class="row pv3">
                <div class="col-xs-4 tl">
                    <a href="https://secure2.mbsbooks.com/employee/IndexInsite.aspx?s=bookstore.canadacollege.edu"
                        target="_blank">© Cañada College</a>
                </div>
                <div class="col-xs-8 tr">
                    <a href="https://goo.gl/maps/EF11JrEz1tN2"><span class="fa fa-map" aria-hidden="true"></span>
                        &nbsp;4200 Farm Hill Blvd., Redwood City, CA</a>
                </div>
            </div>
        </div>
    </div>
</div>
`;

let shopIcons = `
  <li id="login"><a href="#"><svg class="navbar-icon" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shopping-cart" class="svg-inline--fa fa-shopping-cart fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M528.12 301.319l47.273-208C578.806 78.301 567.391 64 551.99 64H159.208l-9.166-44.81C147.758 8.021 137.93 0 126.529 0H24C10.745 0 0 10.745 0 24v16c0 13.255 10.745 24 24 24h69.883l70.248 343.435C147.325 417.1 136 435.222 136 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-15.674-6.447-29.835-16.824-40h209.647C430.447 426.165 424 440.326 424 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-22.172-12.888-41.332-31.579-50.405l5.517-24.276c3.413-15.018-8.002-29.319-23.403-29.319H218.117l-6.545-32h293.145c11.206 0 20.92-7.754 23.403-18.681z"></path></svg><span class="sr-only">Login</span></a></li>
  <li id="shopping-cart" style="margin-left: auto;"><a href="#"><svg class="navbar-icon" aria-hidden="true" focusable="false" data-prefix="far" data-icon="user-circle" class="svg-inline--fa fa-user-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512"><path d="M248 104c-53 0-96 43-96 96s43 96 96 96 96-43 96-96-43-96-96-96zm0 144c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm0-240C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zm0 448c-49.7 0-95.1-18.3-130.1-48.4 14.9-23 40.4-38.6 69.6-39.5 20.8 6.4 40.6 9.6 60.5 9.6s39.7-3.1 60.5-9.6c29.2 1 54.7 16.5 69.6 39.5-35 30.1-80.4 48.4-130.1 48.4zm162.7-84.1c-24.4-31.4-62.1-51.9-105.1-51.9-10.2 0-26 9.6-57.6 9.6-31.5 0-47.4-9.6-57.6-9.6-42.9 0-80.6 20.5-105.1 51.9C61.9 339.2 48 299.2 48 256c0-110.3 89.7-200 200-200s200 89.7 200 200c0 43.2-13.9 83.2-37.3 115.9z"></path></svg><span class="sr-only">Shopping Cart</span></a></li>
`;

$(document).ready(function () {
  jQuery("body > header").append(headerContent);
  $("body > footer").empty();
  $("#div_privacy").empty();
  $("#Server").empty();
  $("body > footer").append(footerContent);
  $("#can-sidenav").append(sideNav);
  $("#myNavbar > ul").append(shopIcons);
});
