# Skyline Bookstore Template Generator
This is a Skyline Bookstore theme CSS generator for new RWD (Responsive Web Design) based on Bootstrap 3.x and Skyline color themes.

- Required Nodejs
- `npm install` for required package installs
- `npm run serve` for development while browsing localhost:8888
- `npm run prod` to generate CSS and javascript in /public/dist folder